variable "some_secret" {
  description = "This is very confidential"
  type = string
  sensitive   = true
}
